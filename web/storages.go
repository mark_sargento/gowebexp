package web

import (
	"bitbucket.org/yml/gowebexp/pages"
	"errors"
	"strconv"
	"strings"
)

// A storage interface provide method to interact with the models
// store in the persistence layer
type Storage interface {
	AddPage(page pages.Page)
	GetPageBySlug(slug string) (pages.Page, error)
	String() string
	CountPage() int
}

// In memory storage
type MemStorage struct {
	Pages []pages.Page
}

// Discuss: Should we return the interface or the address of the MemStorage
// instance
func NewMemStorage() (s Storage, err error) {
	ms := &MemStorage{}
	ms.Init()
	return ms, nil
}

func (ms MemStorage) String() (str string) {
	switch len(ms.Pages) {
	case 0:
		str = "No pages"
	case 1:
		str = "1 page : " + ms.Pages[0].Name
	default:
		var page_names []string
		for _, p := range ms.Pages {
			page_names = append(page_names, p.Name)
		}
		str = strconv.Itoa(len(ms.Pages)) + " pages : " + strings.Join(page_names, ", ")
	}
	return str
}

func (ms *MemStorage) CountPage() int {
	return len(ms.Pages)
}

func (ms *MemStorage) AddPage(page pages.Page) {
	ms.Pages = append(ms.Pages, page)
}

func (ms *MemStorage) GetPageBySlug(slug string) (page pages.Page, err error) {
	for _, page := range ms.Pages {
		if page.Slug == slug {
			return page, nil
		}
	}
	return pages.Page{}, errors.New("storage.GetPageBySlug This page does not exist")
}

// Init Create 2 dummy pages
func (ms *MemStorage) Init() {
	ms.AddPage(pages.Page{
		Name:    "foo",
		Slug:    "foo",
		Content: "# foo\nHello world"})
	ms.AddPage(pages.Page{
		Name:    "bar",
		Slug:    "bar",
		Content: "# bar\nHello world"})
}
